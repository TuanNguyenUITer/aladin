#pragma once
#include "Effect.h"

class EnemyExplosion : public Effect {
public:
	EnemyExplosion(D3DXVECTOR3 pos);
};
