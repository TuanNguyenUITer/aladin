#pragma once
#include "Effect.h"

class ItemExplosion : public Effect {
public:
	ItemExplosion(D3DXVECTOR3 pos);
	~ItemExplosion();
};
