#pragma once
#include "Effect.h"

class SmallItemExplosion : public Effect {
public:
	SmallItemExplosion(D3DXVECTOR3 pos);
};
