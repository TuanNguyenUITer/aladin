#pragma once
#include "Effect.h"

class ThinItemExplosion : public Effect {
public:
	ThinItemExplosion(D3DXVECTOR3 pos);
};
