#pragma once
#include "Effect.h"

class SkeletonWeaponExplosion : public Effect {
public:
	SkeletonWeaponExplosion(D3DXVECTOR3 pos);
};
