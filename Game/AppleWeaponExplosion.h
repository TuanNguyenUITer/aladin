#pragma once
#include "Effect.h"

class AppleWeaponExplosion : public Effect {
public:
	AppleWeaponExplosion(D3DXVECTOR3 pos);
};
