#pragma once
#include "Effect.h"

class BigItemExplosion : public Effect {
public:
	BigItemExplosion(D3DXVECTOR3 pos);
};
