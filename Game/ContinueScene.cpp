﻿#include "ContinueScene.h"
#include "SceneManager.h"


//màn sau RevivingScene nếu hết mạng
ContinueScene::ContinueScene()
{
	LoadContent();
}

ContinueScene::~ContinueScene()
{
	delete backGround;
	backGround = NULL;
}

void ContinueScene::LoadContent()
{
	posBackGround = D3DXVECTOR3(0, 0, 0);
	auto texs = Textures::GetInstance();
	texs->Add(8600, "Resources/Scene/continueScene.png", D3DCOLOR_XRGB(255, 0, 255));
	backGround = new Sprites(texs->GetTexture(8600), BoxCollider());
}

void ContinueScene::Update(float dt)
{
	ProcessInput();
}

void ContinueScene::Render()
{
	backGround->NormalDraw(posBackGround);
}

void ContinueScene::ProcessInput()
{
	auto sceneM = SceneManager::GetInstance();
	KeyBoard* keyboard = KeyBoard::GetInstance();

	if (keyboard->GetKey(RIGHT_ARROW))//yes
	{
		if (!sceneM->isCompleteSultanDungeon)//chưa hoàn thành vòng Sultan
		{
			sceneM->LoadScene(ID_SULTAN_DUNGEON);
			return;
		}

		if (!sceneM->isCompleteScene2 && sceneM->isCompleteSultanDungeon == true)// chưa hoàn thành vòng 2 mà đã xong vòng 1
		{
			sceneM->LoadScene(SCENE_JAFAR_PALACE);
			return;
		}
	}
	else if (keyboard->GetKey(LEFT_ARROW))//no
		sceneM->LoadScene(ID_MENU_SCENE);
}

int ContinueScene::GetSceneID()
{
	return ID_CONTINUE_SCENE;
}
