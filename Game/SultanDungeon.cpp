﻿#include "SultanDungeon.h"
#include "SceneManager.h"
//màn 1
SultanDungeon::SultanDungeon()
{
	LoadContent();
}

SultanDungeon::~SultanDungeon()
{
	delete map;
	map = NULL;
	delete camera;
	camera = NULL;
	delete _wish;
	_wish = NULL;
	delete _wish1;
	_wish1 = NULL;
}
 // load object, map,grid,....
void SultanDungeon::LoadContent()
{
	auto texs = Textures::GetInstance();

	map = new GameMap(ID_SULTAN_DUNGEON, (char*)"Resources/Maps/1tileset.png", (char*)"Resources/Maps/1tilemap.txt", (char*)"Resources/Maps/1gridBuilt.txt", 16, 16);

	int width = Graphic::GetInstance()->GetBackBufferWidth();
	int height = Graphic::GetInstance()->GetBackBufferHeight();

	camera = new Camera(width, height);
	map->SetCamera(camera);

	// Player
	player = new Player();
	//player->SetPosition(1710, 400);
	//player->SetPosition(2000, 1000);
	//player->SetPosition(1000, 600);
	player->SetPosition(100, 100);
	//player->ReloadData();
	player->lastposition = player->GetPosition();
	(new Unit(map->GetGrid(), player))->SetActive(true);//set ô chứa tk player trong vùng ảnh hưởng

	camera->SetPosition(player->GetPosition());//khởi tạo vị trí ban đầu Cam theo player
	CheckCamera();
	data = new Data();

	ObjectPooling* pool = ObjectPooling::GetInstance();
	pool->AddApple();
	pool->AddSkeleton(25);

	srand(time(NULL));

	texs->Add(111111, "Resources/Scene/deal.png", D3DCOLOR_XRGB(255, 0, 255));
	_wish = new Sprites(texs->GetTexture(111111), BoxCollider());
	posWish = D3DXVECTOR3(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 3, 0);

	texs->Add(111112, "Resources/Scene/moreGems.png", D3DCOLOR_XRGB(255, 0, 255));
	_wish1 = new Sprites(texs->GetTexture(111112), BoxCollider());
	posWish1 = D3DXVECTOR3(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 3, 0);
}


void SultanDungeon::Update(float dt)
{
	gameTime += dt;//
	CheckActive();//kiểm tra và active cell trong Cam
	ProcessInput();//xử lý nhập từ bàn phím
	CheckCollision(dt);// Kiểm tra va chạm
	camera->Update(dt);		// Camera follow player
	map->GetGrid()->Update(dt);
	CheckCamera();
	data->Update(dt);
	DrawWish(dt);

	D3DXVECTOR3 playerPos = player->GetPosition();
	if (playerPos.x < 25)
		player->SetPosition(25, playerPos.y);
	else if (playerPos.x > map->GetWidth() - 25)
		player->SetPosition(map->GetWidth() - 25, playerPos.y);

	// suface Data
	// chuyen scene Rviving
	if (isTransition == true)
	{
		SceneManager::GetInstance()->LoadScene(ID_RIVIVING_SCENE);
		return;
	}

	// chuyen Scene
	if (SceneManager::GetInstance()->isEndSultanDungeon == true)
	{
		SceneManager::GetInstance()->LoadScene(ID_COMPLETE_SCENE);
		SceneManager::GetInstance()->SetSceneLv(2);
		return;
	}
}

void SultanDungeon::Render()
{
	if (SceneManager::GetInstance()->isEndSultanDungeon == true)
		return;
	map->Draw();
	map->GetGrid()->Render();
	data->Render();
	if (drawWish == true)
		_wish->NormalDraw(posWish);
	if (drawWish1 == true)
		_wish1->NormalDraw(posWish1);
}

int SultanDungeon::GetSceneID()
{
	return ID_SULTAN_DUNGEON;
}

void SultanDungeon::ProcessInput()
{
	KeyBoard* input = KeyBoard::GetInstance();
	player->HandleInput();
}

//kiểm tra đi lố qua trái, phải 2 góc
void SultanDungeon::CheckCamera()
{
	D3DXVECTOR3 camPos = camera->GetPosition();
	float halfWidth = (float)camera->GetWidth() / 2;
	float halfHeight = (float)camera->GetHeight() / 2;
	auto worldWidth = map->GetWidth();
	auto worldHeight = map->GetHeight();
	if (camPos.x - halfWidth < 0)
		camPos.x = halfWidth;
	if (camPos.x + halfWidth > worldWidth)
		camPos.x = worldWidth - halfWidth;

	if (camPos.y - halfHeight < 0)
		camPos.y = halfHeight;
	if (camPos.y + halfHeight > worldHeight)
		camPos.y = worldHeight - halfHeight;

	camera->SetPosition(camPos);
}

void SultanDungeon::CheckActive()
{
	Entity::MoveDirection camDirection = player->GetVelocity().x > 0 ? Entity::LeftToRight : Entity::RightToLeft;
	map->GetGrid()->HandleActive(camera->GetRect(), camDirection);
}

void SultanDungeon::CheckCollision(float dt)
{
	map->GetGrid()->HandleCollisions(dt);
}

void SultanDungeon::DrawWish(float dt)
{
	if (drawWish)
		posWish = D3DXVECTOR3(posWish.x - RUN_SPEED * dt, posWish.y, 0);
	if (drawWish1)
		posWish1 = D3DXVECTOR3(posWish1.x - RUN_SPEED * dt, posWish1.y, 0);
	if (posWish.x + _wish->GetWidth() < 0)
	{
		posWish = D3DXVECTOR3(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 3, 0);
		drawWish = false;
	}
	if (posWish1.x + _wish1->GetWidth() < 0)
	{
		posWish1 = D3DXVECTOR3(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 3, 0);
		drawWish1 = false;
	}
}
